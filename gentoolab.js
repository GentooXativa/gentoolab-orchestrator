const debug = require('debug')('gentoolab:main');

require('dotenv').config();

const express = require('express');

const promClient = require('prom-client');

const {
  collectDefaultMetrics, register, Gauge,
} = promClient;

collectDefaultMetrics();

const app = express();
const servicePort = process.env.SERVICE_PORT || 4000;

const { execSync } = require('child_process');
const cron = require('node-cron');

const moment = require('moment');
const { existsSync, readFileSync, writeFileSync } = require('fs');
const dnsController = require('./src/dns/dns-controller');
const telegram = require('./src/telegram');

let bot;

const lastRunFile = process.env.LAST_RUN_FILE || '/tmp/gentoolab.last';
const timeWindow = 1;
const now = moment();

try {
  bot = telegram();
} catch (error) {
  console.warn('Bot has been disabled, notification will not be sended.');
  bot = {
    sendMessage: () => {
      console.warn('Bot has been disabled, notification will not be sended.');
    },
  };
}

const executionMetric = new Gauge({
  name: 'gentoolab_execution_time',
  help: 'Execution time in milliseconds for the whole process',
  labelNames: ['domain'],
});

const checkPublicIp = async () => {
  const publicIpServiceUrl = process.env.PUBLIC_IP_SERVICE;
  let publicIp = 'unknown';

  debug(`Requesting public IP from ${publicIpServiceUrl}`);

  const requestExecutionTime = process.hrtime();

  try {
    publicIp = execSync(`curl -s ${publicIpServiceUrl}`)
      .toString()
      .replace('\n', '');
    debug(`Public IP retrieved: ${publicIp}`);
    const hasBeenUpdated = await dnsController(publicIp);

    if (hasBeenUpdated) {
      bot.sendMessage(
        process.env.TELEGRAM_CHAT_ID,
        `Hola caracola!\nLa IP del dominio ${process.env.MAIN_DOMAIN} ha cambiado:\n${publicIp}\n`,
      );
      writeFileSync(lastRunFile, now.toISOString());
    }
    const elapsedMs = process.hrtime(requestExecutionTime)[1] / 1000000;
    executionMetric.set({ domain: process.env.MAIN_DOMAIN }, elapsedMs);
    register.metrics();
  } catch (error) {
    bot.sendMessage(
      process.env.TELEGRAM_CHAT_ID,
      `Ooops!!! Algo ha fallado al actualizar las DNS de OVH para el dominio: ${
        process.env.MAIN_DOMAIN
      }\nLa IP que he conseguido es: ${publicIp}\nTraza del error: ${error.toString()}`,
    );

    console.dir(error);
  }
};

const checkLastSuccessRun = () => {
  if (existsSync(lastRunFile)) {
    const fileData = readFileSync(lastRunFile).toString();
    const diff = moment(fileData).diff(now, 'h');
    if (diff >= 1) {
      return false;
    }
    return true;
  }
  return false;
};

cron.schedule('*/5 * * * *', async () => {
  console.log('Checking public IP');
  await checkPublicIp();
  console.log('Checking public IP ended');
});

app.get('/metrics', async (req, res) => {
  const metrics = await promClient.register.metrics();
  res.send(metrics);
});

app.listen(servicePort, () => {
  debug(`Application started on port ${servicePort}`);
  bot.sendMessage(
    process.env.TELEGRAM_CHAT_ID,
    `Orchestrator started for domain: ${
      process.env.MAIN_DOMAIN
    }`,
  );
  checkPublicIp();
});
