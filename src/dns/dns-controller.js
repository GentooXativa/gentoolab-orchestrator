const debug = require('debug')('gentoolab:dns-controller');
const moment = require('moment');
const {
  retrieveDomains,
  retrieveDomainZone,
  retrieveZoneRecords,
  retrieveZoneRecordInfo,
  updateWildcardRecord,
  updateMainRecord,
  refreshZone,
} = require('./ovh-dns');

const dumpZoneInfo = async (zoneName) => {
  const zone = await retrieveDomainZone(zoneName);
  const lastUpdate = moment(zone.lastUpdate);
  debug(
    `Zone ${zoneName.padEnd(24, ' ')} | Last update: ${lastUpdate.format(
      'DD/MM/YYYY hh:mm:ss',
    )} | NS: ${zone.nameServers.join(', ')}`,
  );

  return zone;
};

module.exports = async (publicIp) => {
  const domains = await retrieveDomains();

  const zoneDump = domains.map((domain) => {
    return dumpZoneInfo(domain);
  });

  const zones = await Promise.all(zoneDump);

  const mainDomain = zones.find((z) => z.name === process.env.MAIN_DOMAIN);

  if (!mainDomain) {
    throw new Error(`Main domain not found: ${process.env.MAIN_DOMAIN}`);
  }

  const records = await retrieveZoneRecords(mainDomain.name);
  const recordsData = await Promise.all(
    records.map((r) => retrieveZoneRecordInfo(mainDomain.name, r)),
  );

  recordsData.forEach((record) => {
    debug(
      `Record ${record.id} | Type: ${record.fieldType.padStart(
        5,
        ' ',
      )} | Subdomain: ${record?.subDomain?.padEnd(
        20,
        ' ',
      )} | Target: ${record.target.substr(0, 50)}`,
    );
  });

  const wildcardRecord = recordsData.find((r) => r.subDomain === '*');
  const mainRecord = recordsData.find(
    (r) => r.subDomain === '' && r.fieldType === 'A',
  );

  if (!wildcardRecord) {
    throw new Error(
      `Wildcard subdomain not found, creation not implemented yet!`,
    );
  }

  if (!mainRecord) {
    throw new Error(`Main subdomain not found, creation not implemented yet!`);
  }

  if (wildcardRecord.target !== publicIp) {
    console.log(
      `Public IP has changed since last check | Previous: ${wildcardRecord.target} | Current: ${publicIp}`,
    );

    try {
      await updateMainRecord(mainDomain.name, mainRecord.id, publicIp);
      debug(`Wildcard record has been updated!`);

      await updateWildcardRecord(mainDomain.name, wildcardRecord.id, publicIp);
      debug(`Wildcard record has been updated!`);
      await refreshZone(mainDomain.name);
      return true;
    } catch (error) {
      throw error;
    }
  } else {
    debug(`Wildcard subdomain points to our public IP, nothing to do :)`);
    return false;
  }
};
