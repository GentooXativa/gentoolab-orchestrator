const debug = require('debug')('gentoolab:ovh-dns');

const ovh = require('ovh')({
  endpoint: process.env.OVH_APP_ENDPOINT,
  appKey: process.env.OVH_APP_KEY,
  appSecret: process.env.OVH_APP_SECRET,
  consumerKey: process.env.OVH_APP_CONSUMERKEY,
});

const retrieveDomains = () =>
  new Promise((resolve, reject) => {
    debug(`Retrieving domains`);
    ovh.request('GET', '/domain', {}, function (error, domains) {
      if (error) {
        reject(error);
      } else {
        resolve(domains);
      }
    });
  });

const retrieveDomainZone = (domain) =>
  new Promise((resolve, reject) => {
    debug(`Retrieving zones for domain: ${domain}`);
    ovh.request('GET', `/domain/zone/${domain}`, {}, function (error, zone) {
      if (error) {
        reject(error);
      } else {
        resolve(zone);
      }
    });
  });

const retrieveZoneRecords = (domain) =>
  new Promise((resolve, reject) => {
    debug(`Retrieving records for domain: ${domain}`);
    ovh.request(
      'GET',
      `/domain/zone/${domain}/record`,
      {},
      function (error, records) {
        if (error) {
          reject(error);
        } else {
          resolve(records);
        }
      },
    );
  });

const refreshZone = (domain) =>
  new Promise((resolve, reject) => {
    debug(`Refreshing domain: ${domain}`);
    ovh.request(
      'POST',
      `/domain/zone/${domain}/refresh`,
      {},
      function (error, records) {
        if (error) {
          reject(error);
        } else {
          resolve(records);
        }
      },
    );
  });

const retrieveZoneRecordInfo = (domain, zoneId) =>
  new Promise((resolve, reject) => {
    debug(`Retrieving records for domain: ${domain} and zone ${zoneId}`);
    ovh.request(
      'GET',
      `/domain/zone/${domain}/record/${zoneId}`,
      {},
      function (error, zoneInfo) {
        if (error) {
          reject(error);
        } else {
          resolve(zoneInfo);
        }
      },
    );
  });

const updateWildcardRecord = (domain, recordId, targetIp) =>
  new Promise((resolve, reject) => {
    debug(`Setting wildcard record for ${domain} to ip: ${targetIp}`);
    ovh.request(
      'PUT',
      `/domain/zone/${domain}/record/${recordId}`,
      {
        subDomain: '*',
        ttl: 60,
        target: targetIp,
      },
      function (error, zoneInfo) {
        if (error) {
          reject(error);
        } else {
          resolve(zoneInfo);
        }
      },
    );
  });

const updateMainRecord = (domain, recordId, targetIp) =>
  new Promise((resolve, reject) => {
    debug(`Setting main record for ${domain} to ip: ${targetIp}`);
    ovh.request(
      'PUT',
      `/domain/zone/${domain}/record/${recordId}`,
      {
        subDomain: '',
        ttl: 60,
        target: targetIp,
      },
      function (error, zoneInfo) {
        if (error) {
          reject(error);
        } else {
          resolve(zoneInfo);
        }
      },
    );
  });

module.exports = {
  retrieveDomains,
  retrieveDomainZone,
  retrieveZoneRecords,
  retrieveZoneRecordInfo,
  updateWildcardRecord,
  updateMainRecord,
  refreshZone,
  //   ovh.request(
  //     'POST',
  //     '/auth/credential',
  //     {
  //       accessRules: [
  //         { method: 'GET', path: '/*' },
  //         { method: 'POST', path: '/*' },
  //         { method: 'PUT', path: '/*' },
  //         { method: 'DELETE', path: '/*' },
  //       ],
  //     },
  //     function (error, credential) {
  //       console.log(error || credential);
  //     },
  //   );
};
