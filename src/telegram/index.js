const TelegramBot = require('node-telegram-bot-api');

// replace the value below with the Telegram token you receive from @BotFather
const token = process.env.TELEGRAM_TOKEN;

module.exports = (polling = false) => {
  if (!token) {
    throw new Error(`Telegram token not set!`);
  }
  const bot = new TelegramBot(token, { polling });

  return bot;
};
